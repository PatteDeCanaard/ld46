extends KinematicBody
#script inspired by https://www.youtube.com/watch?v=-CudxS6EeNA

class_name Baby

export var pdv = 20

var gravity = -9.81
var velocity = Vector3()
var character

export var AIR_ACCELERATION = 5.0
export var AIR_DE_ACCELERATION = 25.0
export var MASSE = 10.0
export var JUMP_FORCE = 10.0

var isRunning = false

var estDansLesBrasDePapa = true

var player #: Spatial

export var isOnGround = false

func _ready():
	character = get_node("Mesh")
	player = get_parent()
	if $Control/TextureProgress :
		$Control/TextureProgress.max_value = pdv
		$Control/TextureProgress.value = pdv
	assert(player!=null)

func _physics_process(delta):
	if(!estDansLesBrasDePapa):
		velocity.y += delta*gravity*MASSE
		
	var dir = Vector3.ZERO
	
	if is_on_floor():
		velocity = Vector3.ZERO
		if !isOnGround:
			hit()
			isOnGround = true
	
	var hv = velocity
	hv.y = 0
	
	var speed = velocity.length()
	var new_pos = dir * speed
	var accel = AIR_ACCELERATION
	
	if dir.dot(hv)>0:
		accel = AIR_DE_ACCELERATION
		
	hv = hv.linear_interpolate(new_pos, accel*delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
	
	velocity = move_and_slide(velocity, Vector3(0,1,0))

func jeter(v):
	estDansLesBrasDePapa = false
	velocity = v
	velocity.y += JUMP_FORCE
	
func ramasser():
	isOnGround = false
	estDansLesBrasDePapa = true
	velocity = Vector3.ZERO

func hit():
	$Pouf.play()
	$Pouf2.play()
	if(pdv>0):
		pdv = pdv-1
		$Control/TextureProgress.value = pdv
	if(pdv<=0):
		onDie()
		return
	yield(get_tree().create_timer(0.5), "timeout")
	$Control/TextureProgressBg.value = $Control/TextureProgress.value
	
func onDie():
	$CanvasLayer.show()
	get_tree().paused = true
