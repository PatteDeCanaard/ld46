extends Spatial

var left = true
var playing : int = false

var counter : float = 0
var delay : float = 0.44

var rng : RandomNumberGenerator = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	set_process(false)

func play():
	counter = 0.5
	playing = true
	set_process(true)
	
func stop():
	playing = false
	set_process(false)
	for child in get_children():
		child.stop()

func _process(delta):
	var r : int = rng.randi_range(0,2)
	counter += delta
	if counter >= delay:
		counter = 0
		if left:
			get_child(r).play()
		else :
			get_child(r+3).play()
		left = not left
