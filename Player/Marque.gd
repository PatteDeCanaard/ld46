extends Spatial


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
onready var baby : Baby= get_tree().get_root().find_node("Baby",true,false)

func active(b):
	if b:
		$Fleche.visible = true
	else:
		$Fleche.visible = false

var of1 = false
var of2 = false
func _process(delta: float) -> void:
	if !baby.estDansLesBrasDePapa:
		if($RayCast.is_colliding()):
			var p = $RayCast.get_collision_point()
			$Fleche.global_transform.origin = p
		if baby.is_on_floor() or of1 or of2:
			$Fleche.global_transform.origin = baby.global_transform.origin - Vector3.UP
		of2 = of1
		of1 = baby.is_on_floor() 
