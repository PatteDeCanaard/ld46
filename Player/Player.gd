extends KinematicBody

#script inspired by https://www.youtube.com/watch?v=-CudxS6EeNA

class_name Player

var gravity = -9.81
var velocity = Vector3()
var camera
var character
var is_moving = false

export var SPEED = 13.0
export var ACCELERATION = 5.0
export var DE_ACCELERATION = 25.0
export var MASSE = 10.0
export var JUMP_FORCE = 10.0

var isRunning = false

onready var baby = get_tree().get_root().find_node("Baby",true,false)
var hasBaby = true
var peutRamasserBaby = false
var initBabyTransform

var isJumping = false

var canJumpOnBall = true
var onBall = false
export var ballOffset = 3.0
var initPosDustTrail = Vector3.ZERO

func _ready():
	character = get_node("Mesh")
	character.get_node("AreaRecoltBaby").connect("body_entered",self,"ramasser_baby")
	initBabyTransform = baby.transform
	initPosDustTrail = $Mesh/DustTrail.translation
# warning-ignore:return_value_discarded
	$AreaGoOnBall.connect("body_entered",self,"goOnBall")
	$Music.play()
	lastCheckPoint = global_transform.origin

func goOnBall(b):
	if b is Ball and canJumpOnBall:
		setBall(b)

func _physics_process(delta):
	if(isJumping and is_on_floor() and velocity.y<10):
		isJumping = false
		$DustLanding.restart()
		$DustLanding.emitting = true
		$Mesh/DustTrail.emitting = true
	if(onBall):
		ball(delta)
	else:
		walk(delta)


func _process(delta: float) -> void:
	if(Input.is_action_just_pressed("reset")):
		goToLastCheckPoint()
		
	if shake:
		$Target/Camera.set_translation($Target/Camera.get_translation()+Vector3(rand_range(-1.0, 1.0) *  shake_amplitude , rand_range(-1.0, 1.0) *  shake_amplitude,0 ))
		shake_time -= delta
		if shake_time<0:
			#$ViewportRender.position = shake_init
			$Target/Camera.set_translation(shake_init)
			shake = false

onready var ball_rb
func setBall(b):
	onBall = true
	isJumping = false
	canJumpOnBall = false
	ball_rb = b
	$Mesh/DustTrail.emitting = true
	$Mesh/DustTrail.global_transform.origin=$Mesh/DustTrail.global_transform.origin-Vector3(0,ballOffset,0)
	
# warning-ignore:unused_argument
func ball(delta):
	global_transform.origin = ball_rb.global_transform.origin+Vector3(0,ballOffset,0)
	camera = get_node("Target/Camera").get_global_transform()
	var ball = ball_rb
	if ball is RigidBody:
		var dir = Vector3()
		is_moving = false	
		if Input.is_action_pressed("top"):
			dir += Vector3(-1,0,0)
			is_moving = true
		if Input.is_action_pressed("down"):
			dir += Vector3(1,0,0)
			is_moving = true
		if Input.is_action_pressed("left"):
			dir += Vector3(0,0,1)
			is_moving = true
		if Input.is_action_pressed("right"):
			dir +=  Vector3(0,0,-1)
			is_moving = true
		dir = dir.normalized()
		ball.add_force(dir*20.0,ball.transform.origin+Vector3.UP)
		if(ball.linear_velocity.length()>20.0):
			ball.linear_velocity = ball.linear_velocity.normalized()*20.0
		if(ball.angular_velocity.length()>10):
			ball.angular_velocity = ball.angular_velocity.normalized()*10
		
		#rotation xz
		var v1 = ball.linear_velocity.normalized()
		var v2 = dir
		var d = -1*v1.dot(v2)
		var anglex = d*15
		var char_rot = character.get_rotation()
		char_rot.x = deg2rad(anglex)
		character.set_rotation(char_rot)
		
		#rotation y
		if is_moving:
			var angle = atan2(ball.linear_velocity.x,ball.linear_velocity.z)
			char_rot = character.get_rotation()
			char_rot.y = angle
			character.set_rotation(char_rot)
		

		isRunning = ball.angular_velocity.length()>2.0
		setAnimAndSound()
			
		#exit ball
		if(Input.is_action_just_pressed("jump")):
			onBall = false
			isJumping = true
			$Mesh/player_anims/AnimationPlayer.seek(0)
			$Mesh/player_anims/AnimationPlayer.play("jump2")
			$Mesh/DustTrail.translation=initPosDustTrail
			$Mesh/DustTrail.emitting = false
			dir = Vector3.ZERO
			if Input.is_action_pressed("top"):
				dir += camera.basis[1]
				is_moving = true
			if Input.is_action_pressed("down"):
				dir += -camera.basis[1]
				is_moving = true
			if Input.is_action_pressed("left"):
				dir += - camera.basis[0]
				is_moving = true
			if Input.is_action_pressed("right"):
				dir +=  camera.basis[0]
				is_moving = true
			velocity = dir * SPEED
			velocity.y = JUMP_FORCE
			char_rot = character.get_rotation()
			char_rot.x = 0
			char_rot.z = 0
			character.set_rotation(char_rot)
			jeter_baby()
			yield(get_tree().create_timer(0.7), "timeout")
			canJumpOnBall = true


func walk(delta):
	camera = get_node("Target/Camera").get_global_transform()
	
	var dir = Vector3()
	
	is_moving = false
	
	if Input.is_action_pressed("top"):
		dir += camera.basis[1]
		is_moving = true
	if Input.is_action_pressed("down"):
		dir += -camera.basis[1]
		is_moving = true
	if Input.is_action_pressed("left"):
		dir += - camera.basis[0]
		is_moving = true
	if Input.is_action_pressed("right"):
		dir +=  camera.basis[0]
		is_moving = true
		
	dir.y = 0
	dir = dir.normalized()
	
	velocity.y += delta*gravity*MASSE
	
#	if(is_on_floor()):
#		velocity += get_floor_velocity()
	
	var hv = velocity
	hv.y = 0
	
	var new_pos = dir * SPEED
	var accel = DE_ACCELERATION
	
	if dir.dot(hv)>0:
		accel = ACCELERATION
		
	hv = hv.linear_interpolate(new_pos, accel*delta)
	
	velocity.x = hv.x
	velocity.z = hv.z
	
	setAnimAndSound()
	if(is_moving and !isRunning):
		isRunning = true
		#get_node("AnimationPlayer").play("run")
	if(!is_moving and isRunning):
		isRunning = false
		#get_node("AnimationPlayer").play("idle")
		
	if(Input.is_action_just_pressed("jump") && is_on_floor()):
		jump(JUMP_FORCE)
		
#	if(!is_moving):
#		var char_rot = character.get_rotation()
#		var vz = character.transform.basis.z.normalized()
#		var v = get_floor_velocity()
#		if(v.length()>0):
#			var a = atan(v.length()/vz.length())
#			#var a = atan2(v.x,v.z)
#			char_rot.y += a*delta
#			character.set_rotation(char_rot)
#
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	if is_moving:
		var angle = atan2(hv.x,hv.z)
		var char_rot = character.get_rotation()
		char_rot.y = angle
		character.set_rotation(char_rot)

func setAnimAndSound():
	var footsteps = $Sounds
	if(isRunning):
		$Mesh/DustTrail.emitting = !isJumping
		if !footsteps.playing and !isJumping:
			footsteps.play()
		elif isJumping:
			footsteps.stop()
		if hasBaby:
			$Mesh/player_anims/AnimationPlayer.play("carryingRun")
		else:
			if isJumping:
				return
			$Mesh/player_anims/AnimationPlayer.play("run")
	if(!isRunning or isJumping):
		$Mesh/DustTrail.emitting = false
		footsteps.stop()
		if isJumping:
			return
		if hasBaby:
			$Mesh/player_anims/AnimationPlayer.play("carrying")
		else:
			if isJumping:
				return
			$Mesh/player_anims/AnimationPlayer.play("idle")
		
func jeter_baby(v = Vector3(0,0,0)):
	if(hasBaby):
		baby.get_node("CollisionShape").disabled = false
		hasBaby = false
		baby.get_node("Marque").active(true)
		var t = baby.global_transform
		character.remove_child(baby)
		get_parent().add_child(baby)
		baby.global_transform = t
		baby.jeter(velocity+v)
		setAnimAndSound()
		yield(get_tree().create_timer(0.5), "timeout")
		peutRamasserBaby = true
	
func ramasser_baby(b):
	if b == baby && !hasBaby && peutRamasserBaby:
		baby.get_node("CollisionShape").disabled = true
		baby.get_node("Marque").active(false)
		peutRamasserBaby = false
		hasBaby = true
		get_parent().remove_child(baby)
		character.add_child(baby)
		baby.transform = initBabyTransform
		baby.ramasser()
		setAnimAndSound()

var shake : bool
var shake_amplitude = 1
var shake_delay = 1
var shake_time = 1
var shake_init = Vector3()
func screenshake(amplitude, delay):
	shake_delay = delay
	shake_time = delay
	shake_amplitude = amplitude
	shake = true
	shake_init = $Target/Camera.get_translation()
	pass

func jump(f):
	isJumping = true
	velocity.y = f
	$Mesh/DustTrail.emitting = false
	$Mesh/player_anims/AnimationPlayer.seek(0)
	$Mesh/player_anims/AnimationPlayer.play("jump2")
	jeter_baby()

var lastCheckPoint = Vector3.ZERO

func goToLastCheckPoint():
	global_transform.origin = lastCheckPoint
	ramasser_baby(baby)
