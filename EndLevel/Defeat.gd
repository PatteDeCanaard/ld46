extends CanvasLayer

var clicked : bool

func _ready():
	clicked = false
	hide()
	$NextLevelButton.connect("pressed",self,"_on_next_level_pressed")
	
func _on_next_level_pressed():
	if not clicked:
		$LoadingProgress.show()
		Loader.reload()
		clicked = true

func show():
	for child in get_children():
		if child.name != "LoadingProgress":
			child.show()

func hide():
	for child in get_children():
		child.hide()
