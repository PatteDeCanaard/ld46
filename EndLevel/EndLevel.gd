extends Spatial


onready var daddy  = get_tree().get_root().find_node("Player",true,false)
onready var baby = get_tree().get_root().find_node("Baby",true,false)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area.connect("body_entered",self,"enter")
	$Area.connect("body_exited",self,"exit")

func _on_end_level():
	if baby.estDansLesBrasDePapa :
		$CanvasLayer.show()
		daddy.get_node("Applaudir").play()
		get_tree().paused = true
	else :
		$AnimationPlayer2.play("no_baby")
			
func _process(delta: float) -> void:
	if inBox:
		_on_end_level()
	
var inBox = false
func enter(b):
	if b is Player:
		print("in")
		inBox = true
func exit(b):
	if b is Player:
		print("out")
		inBox = false
	
