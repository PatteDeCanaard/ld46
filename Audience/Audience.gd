extends StaticBody

var audience_male = load("res://Assets/Audience/AudienceMale.tscn")
var audience_female = load("res://Assets/Audience/AudienceFemale.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	spawn_audience()

func spawn_audience():
	for spawner in $Spawners.get_children():
		rng.randomize()
		var gender = rng.randi_range(1, 2) 
		var spawned_character
		
		if (gender == 1):	
			spawned_character = audience_male.instance()
		else:
			spawned_character = audience_female.instance()
		
		spawner.add_child(spawned_character)
	
