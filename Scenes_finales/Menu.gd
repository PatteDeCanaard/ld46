extends Control

const GRAPHICS_LOW : int = 0
const GRAPHICS_MEDIUM : int = 1
const GRAPHICS_HIGH : int = 2

var clicked : bool = false

func _ready():
	clicked = false
	get_node("/root/Menu/Baby").estDansLesBrasDePapa = false
	$HBoxContainer/Options.add_item ("low",0)
	$HBoxContainer/Options.add_item ("medium",1)
	$HBoxContainer/Options.add_item ("high",2)
	$HBoxContainer/Options.select(1)
	_on_option_changed(1)
	$TextureButton.connect("pressed",self,"_on_start_game")
	$HBoxContainer/Options.connect("item_selected",self,"_on_option_changed")

func _on_option_changed(i : int):
	match i:
		GRAPHICS_LOW:
			ProjectSettings.set_setting("rendering/quality/directional_shadow/size",2048)
			ProjectSettings.set_setting("rendering/quality/shadow_atlas/size",2048)
			ProjectSettings.set_setting("rendering/quality/filters/anisotropic_filter_level",2)
			ProjectSettings.set_setting("rendering/quality/shadows/filter_mode",0)
			get_viewport().msaa = Viewport.MSAA_2X
			#ProjectSettings.set_setting("rendering/quality/filters/msaa",1)
		GRAPHICS_MEDIUM:
			ProjectSettings.set_setting("rendering/quality/directional_shadow/size",4096)
			ProjectSettings.set_setting("rendering/quality/shadow_atlas/size",4096)
			ProjectSettings.set_setting("rendering/quality/filters/anisotropic_filter_level",4)
			ProjectSettings.set_setting("rendering/quality/shadows/filter_mode",1)
			get_viewport().msaa = Viewport.MSAA_8X
			
		GRAPHICS_HIGH:
			ProjectSettings.set_setting("rendering/quality/directional_shadow/size",8192)
			ProjectSettings.set_setting("rendering/quality/shadow_atlas/size",8192)
			ProjectSettings.set_setting("rendering/quality/filters/anisotropic_filter_level",16)
			ProjectSettings.set_setting("rendering/quality/shadows/filter_mode",2)
			get_viewport().msaa = Viewport.MSAA_16X

func _on_start_game():
	if not clicked:
		$LoadingLabel.text = "Loading (0s)"
		$LoadingProgress.show()
		Loader.load_game()
		clicked = true
