extends Node

onready var root = get_tree().get_root()


var loading_progress
var timer_text

var load_thread : Thread 
var loader : ResourceInteractiveLoader

var timer : float = 0

var current_level : int = -1
export var levels : Array = ["res://Scenes_finales/Levels/Level0.tscn","res://Scenes_finales/Levels/Level1.tscn",
							"res://Scenes_finales/Levels/LevelTrampo1.tscn","res://Scenes_finales/Levels/LevelBoules1.tscn",
							"res://Scenes_finales/Levels/Guigouvel.tscn","res://Scenes_finales/Levels/LevelCanon.tscn",
							"res://Scenes_finales/Levels/LevelT.tscn","res://Scenes_finales/Menu.tscn"]

func _ready():
	OS.window_fullscreen = false
	set_process(false)

func update_progress():
	var progress = float(loader.get_stage()*100) / loader.get_stage_count()
	# Update your progress bar?
	loading_progress.value = progress

func _process(delta):
	timer += delta
	timer_text.set_text("Loading... "+str(int(timer))+"s")

func reload():
	loading_progress = root.find_node("LoadingProgress",true,false)
	timer_text = root.find_node("LoadingLabel",true,false)
	set_process(true)
	load_thread = Thread.new()
	load_thread.start(self,"load_thread_func")

func load_game():
	current_level = fmod(current_level + 1,levels.size()) 
	loading_progress = root.find_node("LoadingProgress",true,false)
	timer_text = root.find_node("LoadingLabel",true,false)
	set_process(true)
	load_thread = Thread.new()
	load_thread.start(self,"load_thread_func")

func load_thread_func(userdata):
	loader = ResourceLoader.load_interactive(levels[current_level])
	if loader == null: # check for errors
		print("Error: could not create loader.")
		return
	var err = loader.poll()
	while err != ERR_FILE_EOF: # Finished loading.
		if err == OK:
			update_progress()
		else: # error during loading
			print("Error during loading.")
			loader = null
			return
		err = loader.poll()
	
	var resource = loader.get_resource()
	loader = null
	get_tree().change_scene_to(resource)
	set_process(false)
	get_tree().paused = false
	return

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if not OS.window_fullscreen:
			OS.window_fullscreen = true
		else :
			OS.window_fullscreen = false
