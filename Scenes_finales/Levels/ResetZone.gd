extends Area


onready var player = get_tree().get_root().find_node("Player",true,false)
onready var baby = get_tree().get_root().find_node("Baby",true,false)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered",self,"reset")
	pass # Replace with function body.


func reset(b):
	player.goToLastCheckPoint()
