extends SpotLight

onready var player = get_tree().get_root().find_node("Player",true,false)

func _process(delta):
	look_at(player.transform.origin, Vector3.UP)
