extends Spatial

class_name Trampoling

export var force = 30.0

func _ready() -> void:
	$AreaRebond.connect("body_entered",self,"rebond")


func rebond(b):
	if (b is Baby) or (b is Player):
		$Trampoline/AnimationPlayer.play("ArmatureAction",-1,2)
		$BoingSound.play()
		if (b is Baby):
			b.velocity.y = force
		if(b is Player):
			b.jump(force)
