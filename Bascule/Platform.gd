extends KinematicBody


export var speed = 10.0
export var dir = Vector3(0,0,-1)

var initpos
var finalpos
var aller = true

var new_pos
var velocity = Vector3.ZERO

var initp1 = Vector3.ZERO
var initp2 = Vector3.ZERO
var initp3 = Vector3.ZERO
var initp4 = Vector3.ZERO
onready var line1 =  get_parent().find_node("Line3D1",true,false)
onready var line2 =  get_parent().find_node("Line3D2",true,false)
onready var line3 =  get_parent().find_node("Line3D3",true,false)
onready var line4 =  get_parent().find_node("Line3D4",true,false)

export var lineY = 10

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	initpos = translation
	finalpos = translation+dir
	new_pos = (translation+dir)#*0.5+Vector3(0,-1,0)
	initp1 = line1.to_local(to_global($p1.translation))+Vector3(0,lineY,0)+dir*0.5
	initp2 = line1.to_local(to_global($p2.translation))+Vector3(0,lineY,0)+dir*0.5
	initp3 = line1.to_local(to_global($p3.translation))+Vector3(0,lineY,0)+dir*0.5
	initp4 = line1.to_local(to_global($p4.translation))+Vector3(0,lineY,0)+dir*0.5
	pass # Replace with function body.


func setPoint():
	var p1 = line1.to_local(to_global($p1.translation))
	var p2 = initp1
	line1.points = [p1,p2]
	p1 = line2.to_local(to_global($p2.translation))
	p2 = initp2
	line2.points = [p1,p2]
	p1 = line3.to_local(to_global($p3.translation))
	p2 = initp3
	line3.points = [p1,p2]
	p1 = line4.to_local(to_global($p4.translation))
	p2 = initp4
	line4.points = [p1,p2]
	
func _process(delta: float) -> void:
	setPoint()

export var yAccel = 0.01

func _physics_process(delta):
	var dir2 = dir.normalized()
	
	if !aller:
		dir2 = -dir2
	
	#aller/retour
	if aller and (finalpos-initpos).dot(finalpos-translation)<0:
		aller = false
		new_pos = initpos
	
	if !aller and (initpos-finalpos).dot(initpos-translation)<0:
		aller = true
		new_pos = finalpos
	
	var d = dir2 * speed ;
	velocity = velocity.linear_interpolate(d, yAccel*delta)
	
	move_and_slide(velocity,Vector3(0,1,0))
