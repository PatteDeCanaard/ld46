extends ImmediateGeometry

var points = []

export var radius = 5.0
export var vertices = 3.0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func normal(v : Vector3):
	return Vector3(-v.y,v.x,0)

func _process(delta):
	var cylinders = []
	var normals = []
	var vertices_arr = []
	var normals_arr = []
	#fill cylinders
	for i in range(0,points.size(),1):
		var p0 : Vector3 = points[i-1] if i-1>=0 else points[0]
		var p1 : Vector3 = points[i]
		var p2 : Vector3 = points[i + 1] if i+1<points.size() else points[points.size()-1]
		
		var axis01 = (p1-p0).normalized()
		var n01 = normal(axis01)
		var axis12 = (p2-p1).normalized()
		var n12 = normal(axis12)
		
		var n = ((n01+n12)/2.0).normalized()*radius;
		var axis = normal(n).normalized()

		var cylinder = []
		var cylinder_n = []
		for j in range(0,vertices):
			var v = n.rotated(axis,(2*PI/vertices)*j)
			cylinder.push_back(p1+v)
			cylinder_n.push_back(v.normalized())
		cylinder.push_back(cylinder[0])
		cylinder_n.push_back(cylinder_n[0])
		
		cylinders.push_back(cylinder)
		normals.push_back(cylinder_n)
			
	#fill vertices
	for i in range(cylinders.size()-1):
		if i+1<cylinders.size():
			for j in range(cylinders[i].size()-1):
				var v1 : Vector3 = cylinders[i][j]
				var v2 : Vector3 = cylinders[i][j + 1]
				var v3 : Vector3 = cylinders[i+1][j]
				var v4 : Vector3 = cylinders[i+1][j + 1]
				vertices_arr.push_back(v1)
				vertices_arr.push_back(v2)
				vertices_arr.push_back(v3)
				vertices_arr.push_back(v3)
				vertices_arr.push_back(v2)
				vertices_arr.push_back(v4)
				var n1 : Vector3 = normals[i][j]
				var n2 : Vector3 = normals[i][j + 1]
				var n3 : Vector3 = normals[i+1][j]
				var n4 : Vector3 = normals[i+1][j + 1]
				normals_arr.push_back(n1)
				normals_arr.push_back(n2)
				normals_arr.push_back(n3)
				normals_arr.push_back(n3)
				normals_arr.push_back(n2)
				normals_arr.push_back(n4)
				
	clear()
	begin(Mesh.PRIMITIVE_TRIANGLES, null)	
	for i in range(vertices_arr.size()) :
		set_normal(normals_arr[i])
		add_vertex(vertices_arr[i])
	end()
