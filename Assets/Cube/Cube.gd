extends KinematicBody


export var speed : int = 100
export var angle_rotation : float = 90
export var delay : float = 0.5

var axis : Array = [Vector3(1,0,0),Vector3(0,0,1),-Vector3(1,0,0),-Vector3(0,0,1)]

var remaining_angle : float = angle_rotation
var target_axis : Vector3 = Vector3(1,0,0)
var rng = RandomNumberGenerator.new()
var counter : float = 0

func _ready():
	rng.randomize()

func _process(delta):
	if counter >= delay:
		var r = delta*speed if delta*speed < remaining_angle else remaining_angle
		rotate(target_axis,deg2rad(r))
		remaining_angle -= r
		if remaining_angle == 0:
			counter = 0
			target_axis = axis[rng.randi_range(0,axis.size()-1)]
			remaining_angle = angle_rotation
	else :
		counter += delta

