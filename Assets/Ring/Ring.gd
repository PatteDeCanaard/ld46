extends StaticBody


func _ready():
	$Area.connect("body_entered",self,"_on_ring_entered")
	
func _on_ring_entered(body):
	$Particles.emitting = true
	$Sounds/applause.play()
