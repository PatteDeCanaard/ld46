shader_type spatial;

render_mode diffuse_toon, specular_schlick_ggx;

uniform float dissolution : hint_range(0,1);
uniform vec4 color : hint_color;
uniform vec4 color2 : hint_color;

vec2 hash(vec2 p) {
  p = vec2( dot(p,vec2(127.1,311.7)),
        dot(p,vec2(269.5,183.3)) );
 
  return -1.0 + 2.0*fract(sin(p)*43758.5453123);
}

float gnoise(vec2 p) {
  vec2 i = floor( p );
  vec2 f = fract( p );
   
  vec2 u = f*f*(3.0-2.0*f);
 
  return mix( mix( dot( hash( i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ),
           dot( hash( i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
          mix( dot( hash( i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ),
           dot( hash( i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);
}

float pnoise(vec2 p,float amplitude,float frequency,float persistence, int nboctaves) {
  float a = amplitude;
  float f = frequency;
  float n = 0.0;
 
  for(int i=0;i<nboctaves;++i) {
    n = n+a*gnoise(p*f);
    f = f*2.;
    a = a*persistence;
  }

  return n;
}

void fragment(){
	vec3 n = clamp(vec3(pnoise(UV,2,8,0.2,4)*0.5+0.5),0,1);
	vec4 c = color;
	vec4 c2 = color2;
	float d = 0.2;
	float dissolution_d = dissolution * (1.0+d);
	if(n.r < dissolution_d*1.1-d){
		c = c2;
		ALPHA = c.a;
	}
	if(n.r < dissolution_d*1.1 && c.a>0.0 && n.r > dissolution_d*1.1-d){
		c = color2;
		c.a = mix(1,0,clamp(-(n.r-dissolution_d)/d,0,1));
	}
	if(n.r < dissolution_d*1.1 && n.r>dissolution_d*1.1-d*0.5 && c.a>0.0){
		c += color;
	}
	
	ALBEDO = c.rgb;
}