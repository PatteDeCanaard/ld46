extends Spatial

export var delay : float = 1
export var fire_rate : float = 2

onready var boulet = preload("res://Assets/Canon/Boulet.tscn")

var boom : bool = false
var time = 0

func _process(delta: float) -> void:
	if time >= delay or boom:
		boom = true
		if time >= fire_rate:
			$canon/AnimationPlayer.play("ArmatureAction")
			time = 0
			yield(get_tree().create_timer(0.45), "timeout")
			var inst = boulet.instance()
			add_child(inst)
			inst.global_transform = $BouletSpawn.global_transform
			pass
		else:
			time += delta
	else:
		time += delta
	pass
