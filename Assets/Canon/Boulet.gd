extends Spatial


export var direction = Vector3(0,0,1)
export var speed = 5.0

var actif = true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Area.connect("body_entered",self,"destroy")
	$Area.connect("area_entered",self,"destroy_area")
	pass # Replace with function body.



func _process(delta: float) -> void:
	if actif:
		translate(direction*speed*delta)
	pass

func destroy_area(area):
	actif = false
	$AnimationPlayer.play("destroy")
	yield($AnimationPlayer, "animation_finished")
	queue_free()

func destroy(b):
	if b is Player or b is Baby:
		get_tree().get_root().find_node("Player",true,false).jeter_baby(Vector3(rand_range(-10,10),20,rand_range(-10,10)))
	actif = false
	$AnimationPlayer.play("destroy")
	yield($AnimationPlayer, "animation_finished")
	queue_free()
